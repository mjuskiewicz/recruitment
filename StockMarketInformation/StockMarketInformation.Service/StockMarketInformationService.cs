﻿using StockMarketInformation.Contracts;
using System;

namespace StockMarketInformation.Service
{
    public class StockMarketInformationService : IStockMarketInformationService
    {
        public CompanyPriceDetailsResponceDto GetCompanyPriceDetailsForSelectedDate(string companyName, DateTime selectedDate)
        {
            return new CompanyPriceDetailsResponceDto
            {
                CompanyCode = companyName
            };
        }

        public CompanyPriceDetailsResponceDto GetCompanyPriceDetailsForToday(string companyName)
        {
            return GetCompanyPriceDetailsForSelectedDate(companyName, DateTime.Today);
        }
    }
}
