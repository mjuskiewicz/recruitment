﻿using System;
using Microsoft.AspNetCore.Mvc;
using StockMarketInformation.Contracts;

namespace StockMarketInformation.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private readonly IStockMarketInformationService _service;

        public CompanyController(IStockMarketInformationService service)
        {
            _service = service;
        }

        // GET api/values
        [HttpGet("{CompanyCode}/Price/{Date}")]
        public ActionResult<CompanyPriceDetailsResponceDto> GetValuesForSelectedDate(string companyCode, DateTime selectedDate)
        {
            return _service.GetCompanyPriceDetailsForSelectedDate(companyCode, selectedDate);
        }
    }
}
