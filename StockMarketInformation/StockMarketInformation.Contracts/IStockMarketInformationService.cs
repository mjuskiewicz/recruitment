﻿using System;

namespace StockMarketInformation.Contracts
{
    public interface IStockMarketInformationService
    {
        CompanyPriceDetailsResponceDto GetCompanyPriceDetailsForSelectedDate(string companyName, DateTime selectedDate);
    }
}